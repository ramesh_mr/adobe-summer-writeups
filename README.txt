ACM Small Standard Format template, taken from
http://www.acm.org/publications/submissions/latex_style

Original description from ACM's website:

The Small Standard Format is used for the following 
journals and transactions:

CIE, CSUR, JACM, JDIQ, JEA, JETC, TAAS, TACCESS, TACO, TALG, 
TALLIP (formerly TALIP), TECS,TIIS, TISSEC, TIST, TKDD, TMIS, 
TOCE, TOCHI, TOCL, TOCS, TOCT, TODAES, TODS, TOIS, TOIT, TOMACS, 
TOMM (formerly TOMCCAP), TOMPECS, TOMS, TOPC, TOPLAS, TOS, TOSEM, 
TOSN, TRETS, TSAS, TSLP, TWEB