\documentclass[12pt,nopagetitle]{article}
\usepackage{amsfonts, amsmath, amssymb, amsthm, mathtools} 
\usepackage[english]{babel}
\selectlanguage{english}

\usepackage{textcomp, cmap, comment}	                    
\usepackage{graphicx, wrapfig}    
\usepackage{epigraph, color, hyperref}                      
\usepackage{verbatim}
\pagestyle{empty}                                           
\usepackage{array,tabularx,tabulary,booktabs}              
\usepackage{euscript, mathrsfs}	
\usepackage{outlines}
\usepackage{amssymb}% http://ctan.org/pkg/amssymb
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%
\usepackage[utf8]{inputenc}
\usepackage[lastpage,user]{zref}

\usepackage{fancyhdr}
\pagestyle{fancy}


\title{Annotations of dialogue in collaborative image editing domain}


\begin{document}
\maketitle


\begin{abstract}
This document contains the annotation scheme for the image editing dialogue. 
The annotation scheme was developed to support the development of an image editing
conversational agent. 
\end{abstract}

\section{Introduction}

This document explains the annotation scheme for the dialogue between the wizard
and the user for the image editing domain. The dialogue was collected in a wOz
set up between an ``expert'' image editor and ``novice''. We stress on the words
``expert'' and the ``novice'' so that the roles are well defined. The ``expert'' 
wizard in our case operates an image editing program (Adobe lightroom\footnote{https://lightroom.adobe.com})
and the user provides image editing requests and other commands in a conversational
manner to the wizard. 

The conversation collected between the wizard and the user is transcribed with 
timing information. The screen grab of the wizard is also collected so that we
know the actions performed by the wizard. 

\section{Methods}

\subsection{Data}

The conversation data has two parts. i) image locating, ii) image editing. The
image region part is related to the user directing the wizard to select the 
right target image in a selection of about 300 images. The users are provided 
with images that they should be editing and the wizard is not informed before the
experiment about the exact image that the user wants to edit. The annotation scheme
is less about the image locating in the library, but is focued on the image 
editing scheme. The image locating problem in the library is left for the future work. 
The image locating part is annotated with a single ``image locating request'' tag.
We will label the image locating part with the ``ILP''(Image locating part) tag. 

\subsection {Image edit requests/Commands}

This section contains the annotation description for the image edit requests (IER). 

An image edit request is a natural language command issued by the user to change something
in the image as seen. A typical IER contains Action and entities. The entities consist of 
Attribute, Object, region, modifier/value and constraint. 

\begin{table}    
\begin{center}
\begin{tabular}{ | c | m{10cm} |}
\hline
 \textbf{Action} & \textbf{Description} \\ 
\hline
  Delete 
    & encompasses selection and removal of an object in the image  \\ \hline
  Replace 
    & removes a source object, and adds a destination object in the source’s location\\ \hline
  Crop 
    & crops an image \\ \hline
  Add 
    & inserts an object to the existing image \\ \hline
  Adjust 
    & increases or decreases the characteristics of the image, such as saturation or exposure \\ \hline
  Apply 
    & utilizes preset enhancements, such as filters or auto correction, in the image editing tool\\ \hline
  Transform 
    & flips the orientation of an image\\ \hline
  Rotate 
    & changes the orientation of an image by rotating it by a certain degree (such as 90 degrees), or altering the x/y angle offset\\ \hline
  Clone
    & replicates an object already existing in the image\\ \hline
  Select
    & creates a bounding box around an object\\ \hline
  Swap
    & switches locations of two objects without deleting either\\ \hline
  Move
    & relocates an object and needs to be replaced with background\\ \hline
  Merge
    & merges layers\\ \hline
  Undo 
    & undo the previous action(s)\\ \hline
  Redo 
    & redo the previous action(s)\\ \hline
  Zoom 
    & magnifies (zooms in) or zooms out on an image\\ \hline
  Scroll 
    & moves through the viewing space via up, down, left, or right scrolling\\ \hline
  Ambiguous 
    & action is not executable due to unclear wording or requests a change in how the photo was originally taken\\ \hline
\end{tabular}
\caption{\label{tab:ier} Table shows the actions possible in the domain and their explanation. }
\end{center}
\end{table}

\begin{table}    
\begin{center}
\begin{tabular}{ | c | m{10cm} |}
\hline
 \textbf{Entities} & \textbf{Description} \\ 
\hline
  Attribute 
    &  properties of the image to adjust, such as saturation/hue/brightness\\ \hline
  Object 
    &  object specifically requested that will be inserted/removed/transformed\\ \hline
  region 
    &  indicates where in the image an action is being applied, such as top, entire image, or location of a requested object \\ \hline
  Modifier/value 
    &  holds any value specified by user, such as increase/decrease, modifiers of degree (examples: “a little”, “a lot”, “all”), colors, directions (“to the left”), or numerical values\\ \hline
  Intention 
    &  condition which satisfies the intention of the image edit request. For example, in “Paint the rocks unnatural but interesting colors like purple, green, yellow, and red to make the effect surreal”, the constraint is “to make the effect surreal” as it provides insight into the end goal of the user. \\ \hline
\end{tabular}
\caption{\label{tab:ier} Table shows the actions possible in the domain and their explanation. }
\end{center}
\end{table}


\subsection{Image editing}
This part begins once the image to be edited has been selected. The wizard and the
user have a natural conversation and achieve the target goal image edit. 

\section{Annotation labels}

This section shows the labels that are used to annotate the conversation between the
users. 

\subsection{Dialogue acts}

This section describes the dialogue acts for the domain: 

\begin{outline}[enumerate]

\1 IMAGE-LOCATION [IL] : This section of the data deals with the user and the wizard locating the image in the library for editing. 

USED BY: User, Wizard

\1 COMMAND [IER]: Command is the IERs given out by the user. The user's intention of a command is to change something in the image explicitly. General comment on the image doesn't constitute a command. 

USED BY: User

    \2 NEW [N]: This is the beginning of a new Image edit request. A new image edit request happens when the user completes an intended editing goal.
    \2 UPDATE [U]: This relates to the image edit request from the user where the user provides additional context for editing, fills previously unfilled slot-values, changes the user wants to see for a specific IER. 
    \2 COMPARE [C]: This relates to the user asking the user to compare between the edits made. 

\1 COMMENT [COM]: Comment is made either on the image or the edit action performed by the wizard. The comment usually has an implicit intention behind them, but they're not treated explicitly as an IER. The structure of a comment is similar to that of an IER. There are two types of COMMENT that can be made. 

USED BY: User, Wizard

%    \2 ON-IMAGE [I]: The comment can be made on the image on a global level. 
%    \2 ON-EDIT [E]: The comment can be made on the edit specifically. 
    

\1 QUESTION [Q]: The question asked by the user to the wizard. In this domain the questions asked by the user is marked as QUESTION. Those asked by the wizard take a different name. They're assigned different names to distinguish the questions as they have a different intentions behind them with similar use of language. The QUESTION asked by the user are of three types. 

USED BY: User 

    \2 RECOMMENDATION [R]: Asked by the user clarifying what a certain image editing tool is, what it does or if there is a tool for achieving certain high level image editing goal. 
    \2 IMPACT [I]: Question asked on what happens with the use of a certain feature in the tool
    \2 IMAGE-ATTRIBUTE [IA]: Question asked by the user about certain things about the image as a result of Image edit or without. 
    \2 FEEDBACK [FB]: Question asked by the user if the edit is good or bad. 



\1 NARRATE [N]: Any actions, comments, image features etc. described by the wizard is tagged as a NARRATION. 

USED BY:Wizard

\1 CONFIRM [C]: The actions by the user accepting or rejecting the image edits. This can either be to reject the edit/action from the wizard. 

USED BY: User

    \2 ACCEPT [ACC]: Accept the changes from the wizard
    \2 REJECT [REJ]: Reject the changes made by the wizard
    \2 DESCRIBE [D]: Describe the changes made by the wizard without explicitly accepting or rejecting the changes. (Ex: I don't see any difference)

\1 RESPOND [RS]: The responses produced by the user is annotated with a RESPOND tag. The answers to the questions usually have this tag. A response can be also due to the actions performed by the wizard. The answers to questions from the wizard are tagged with a different tag. 

USED BY: USER
    
    \2 YES [Y]: Acknowledgments to the user actions or suggestions. 
    \2 NO [N]: Negative answers to the questions or actions. 
    \2 FEEDBACK [FB]: Feedback from the user about the edit made. This can be responses which are positive, neutral or negative.  
    \2 FINISH [FIN]: Finish the session or the image edit part. 
    \2 DESCRIBE [D]: If the yes or no answers are accompanied with the descriptions they're tagged with this act. They usually accompany yes or no responses but sometimes the user can choose to just provide the description without explicitly giving yes/no answer. 
    \2 ACKNOWLEDGE [ACK]: ?? (Not sure if we need this separately)

\1 REQUEST [RQ]: The requests are (questions as well) made by the wizard to the user. 

USED BY: Wizard

    \2 COMMAND [IER]: Request the command from the user. 
    \2 UPDATE [U]: Request an update for the IER from the user. This is usually used by the wizard when the IER is incomplete.  
    \2 CONFIRM-EDIT [CE]: Request to confirm the edit made by the wizard. 
    \2 FEATURE-PREF [FP]: Request the user if they have any preferred feature to make the edit. This is usually disjunctive questions or Y/N questions. 
    \2 FINISH [FIN]: Ask if the session is over

\1 ELUCIDATE [E]: Elucidation is the tag attached to the responses generated by the wizard. This is so that the responses by the wizard are separated with a separate DA from the user. 

USED BY: Wizard

    \2 YES [Y]: Yes responses. 
    \2 NO [N]:  No responses. 
    \2 DESCRIBE [D]: Describe responses from the wizard. This act like the ones for the user accompanies the yes and no responses. It can also exist all by itself if a response is an explanation without an explicit yes/no response. 
    \2 ACKNOWLEDGE [ACK]: Acknowledge the statement by the user without intending to say yes, no or describe.
    %\2 FEATURE-USE: This is the answer to feature use question asked by the user. 
    %\2 FEATURE-RECOMMENDATION: This is a response to the feature recommendation question by the user. 
    \2 IMAGE-ATTRIBUTE [IA]: This is an answer to the image attribute question by the user. 
    %\2 IMAGE-EDIT [IE]: Utterances from the wizard about the edit. 
    
\1 SUGGEST [S]: This is the act where the wizard is suggesting an image edit or the feature or it's use to the user. This is separated from the 

USED BY: Wizard

    \2 COMMAND [IER]: The wizard is suggesting a new Image edit to the user.
    \2 UPDATE [U]: The wizard suggests an update to the currently operating IER to the user. 
    \2 FEATURE-IMPACT [FI]: The user is suggesting the user as to how the requested feature will change the image. This is either explicitly mentioning the use through words or by demonstration.  

\1 GREETING [GREET]: The greetings by the user and the wizard. 

USED BY: User, Wizard

\1 ECHO [ECHO]: The user and the wizard repeating the utterance from the other. 

USED BY: User, Wizard

\1 DISCOURSE-MARKER [DM]: The discourse markers are those which don't serve any communicative intention but grab the turn from the other. 

USED BY:User, Wizard

\1 Others [O]: Every other DA. Once the image edit session is over, every other intermediary part before the beginning of the next IER is marked as O.  

USED BY: User, Wizard

\end{outline}


\begin{table}    
\begin{center}
\begin{tabular}{ | c | c |  c | c |}
\hline
 \textbf{DA shorthand} & \textbf{Name} & \textbf{User} & \textbf{Wizard} \\ 
\hline
  IL & IMAGE-LOCATION
    & \cmark & \cmark  \\ \hline
  IER-N & NEW COMMAND
    & \cmark & \\ \hline
  IER-U & UPDATE COMMAND
    & \cmark & \\ \hline
  IER-C & COMPARE COMMAND
    & \cmark & \\ \hline
  COM-I & COMMENT-IMAGE
    & \cmark & \cmark \\ \hline
  COM-L & COMMENT LIKE-EDIT
    & \cmark & \cmark \\ \hline
  COM-D & COMMENT DISLIKE-EDIT
    & \cmark & \cmark \\ \hline
  Q-R & QUESTION RECOMMENDATION
    & \cmark & \\ \hline
  Q-I & QUESTION IMPACT
    & \cmark  &  \\ \hline
  Q-IA & QUESTION IMAGE ATTRIBUTE
    &  \cmark &  \\ \hline
  Q-FB & QUESTION FEEDBACK
    & \cmark & \\ \hline
  N & NARRATE
    & & \cmark \\ \hline
  C-ACC & CONFIRM ACCEPT
    & \cmark &  \\ \hline
  C-REJ & CONFIRM REJECT
    & \cmark & \\ \hline
   RS-Y & RESPOND YES
    & \cmark & \\ \hline
   RS-N & RESPOND NO
    & \cmark & \\ \hline
   RS-FB & RESPOND FEEDBACK
    & \cmark & \\ \hline
   RS-FIN & RESPOND FINISH
    & \cmark & \\ \hline
   RS-ACK & RESPOND ACKNOWLEDGE
    & \cmark & \\ \hline
   RQ-IER & REQUEST COMMAND
    & & \cmark \\ \hline
   RQ-U & REQUEST UPDATE
    &  & \cmark \\ \hline
   RQ-CE & REQUEST CONFIRM EDIT
    & & \cmark \\ \hline
   RQ-FP & REQUEST FEATURE PREFERENCE
    & & \cmark \\ \hline
   RQ-FIN & REQUEST FINISH
    & & \cmark \\ \hline
   E-Y & ELUCIDATE YES
    & & \cmark \\ \hline
   E-N & ELUCIATE NO
    & & \cmark \\ \hline
  E-ACK & ELUCIDATE ACKNOWLEDGE
    & & \cmark \\ \hline
  E-IA & ELUCIDATE IMAGE ATTRIBUTE
    & & \cmark \\ \hline
  S-IER & SUGGEST COMMAND
    & & \cmark \\ \hline
  S-U & SUGGEST UPDATE
    & & \cmark \\ \hline
  S-FI & SUGGEST FEATURE IMPACT
    & & \cmark \\ \hline
  GREET & GREETING
    & \cmark & \cmark \\ \hline
  ECHO & ECHO
    & \cmark & \cmark \\ \hline
  DM & DISCOURSE MARKER
    & \cmark & \cmark \\ \hline
  O & OTHERS
    & \cmark & \cmark \\ \hline
\end{tabular}
\caption{\label{tab:da-usage} Table shows the DAs and their usage by either User 
or the wizard}
\end{center}
\end{table}


\begin{table}    
\begin{center}
\begin{tabular}{ | c |  m{10cm} |}
\hline
 \textbf{DA shorthand} & \textbf{Example} \\ 
\hline
   IL & \{all the utterances until the photo is selected\}
   \\ \hline
   IER-N & so lets clear it up or sharpen it
   \\ \hline
   IER-U & a little bit
   \\ \hline
   IER-C & can you undo that change really fat just to see
   \\ \hline
   COM-I & so first of all it is not really clear
   \\ \hline
   COM-L & uh that's better
   \\ \hline
   COM-D & shows nothing
   \\ \hline
   Q-R & Can you suggest me an edit
   \\ \hline
   Q-I & what would that look like
   \\ \hline
   Q-IA & Is that a dog on the left
   \\ \hline
   Q-FB & what do you think
   \\ \hline
   N & Let me increase that and now it's not this one
   \\ \hline
   C-ACC & Yes, Let's keep this
   \\ \hline
   C-REJ & No, Let's not have it
   \\ \hline
   RS-Y & uh huh, yes
   \\ \hline
   RS-N & no
   \\ \hline
   RS-FB & ok
   \\ \hline
   RS-FIN & We're done
   \\ \hline
   RS-ACK & ok
   \\ \hline
   RQ-IER & Do you want to change anything in this photo
   \\ \hline
   RQ-U & more?
   \\ \hline
   RQ-CE & How does this look?
   \\ \hline
   RQ-FP & Do you want to use the contrast or the exposure
   \\ \hline
   RQ-FIN & are we done, do you want to move on to the next photo
   \\ \hline
   E-Y & uh huh, yes
   \\ \hline
   E-N & no
   \\ \hline
  E-ACK & ok
   \\ \hline
  E-IA & there is a dog in the photo
   \\ \hline
  S-IER & do you want to add any vignetting, uh we can try this one for clarity
   \\ \hline
  S-U & A little bit more is good
   \\ \hline
  S-FI & this changes the blueness in the entire picture
   \\ \hline
  GREET & Hello, Good morning
   \\ \hline
  ECHO & \{Repeating what the other person speaks\}
   \\ \hline
  DM & ok, uh, um
   \\ \hline
  O & 
   \\ \hline
\end{tabular}
\caption{\label{tab:da-usage} Table shows the DAs and examples for the same}
\end{center}
\end{table}

\begin{figure}[t]
\centering
\includegraphics[width=\linewidth]{images/example.pdf}
\caption{\label{fig:ex:dialogue} Shows an exmaple dialogue along with the dialogue act and the slots and values for the domain}
\end{figure}
%\section{Statistics}

 
%\begin{thebibliography}{111}
%\end{thebibliography}


\end{document}